-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 21:32 PM
-- Server-versjon: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `city`
--

CREATE TABLE `city` (
  `city` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `firstName` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `lastName` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skierrecord`
--

CREATE TABLE `skierrecord` (
  `userName` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `season` int(10) NOT NULL,
  `club` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city`);

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_city_index` (`city`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skierrecord`
--
ALTER TABLE `skierrecord`
  ADD PRIMARY KEY (`userName`,`season`),
  ADD KEY `skierrecord_club_fk` (`club`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_city_fk` FOREIGN KEY (`city`) REFERENCES `club` (`city`);

--
-- Begrensninger for tabell `skierrecord`
--
ALTER TABLE `skierrecord`
  ADD CONSTRAINT `skierrecord_club_fk` FOREIGN KEY (`club`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `skierrecord_userName_fk` FOREIGN KEY (`userName`) REFERENCES `skier` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
