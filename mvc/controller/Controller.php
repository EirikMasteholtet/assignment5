<?php
include_once("model/DBModel.php");


/**
 * reading xml-data to in-memory-structure
 * delegates the task to the view and the model
 */

class Controller 
{
    public $model;
    public $xmlDoc;

    public function __construct()
    {
        $this->xmlDoc = new DOMDocument();
        $this->xmlDoc->load("../SkierLogs.xml");
        $this->xmlDoc->normalize();
        $this->model = new DBModel($this->xmlDoc);
    }    

    public function invoke()
    {
        try 
        {
            include "view/View.php";
            $this->model->deleteRecords();
            $this->model->extractSkiers(); 
            $this->model->extractClubs();
            $this->model->extractCities();
            $this->model->extractSkierRecord();

        } catch (PDOException $e) {
            echo $e;
        }
    }
}

?>