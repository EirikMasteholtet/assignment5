<?php

class Skier {
    public $userName;
    public $firstName;
    public $lastName;
    public $yearOfBirth;


    /** Constructor
     * @param string $userName Skier username
     * @param string $firstName Skier's firtsname
     * @param string $lastName Skier's lastname
     * @param integer $yearOfBirth Skier's birth year
     */
    public function __construct($userName, $firstName, $lastName, $yearOfBirth)
    {
        $this->userName = $userName;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->yearOfBirth = $yearOfBirth;
    }
}
?>