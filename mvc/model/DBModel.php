<?php
    include_once("DBProp.php");
    include_once("iModel.php");
    include_once("Skier.php");
    include_once("Club.php");
    include_once("City.php");
    include_once("SkierRecord.php");
    
    class DBModel implements iModel
    {
        protected $db = null;
        protected $xpath;

        public function __construct($xmlDoc, $db = null)
        {
            if ($db)
            {
                
                $this->db = $db;
            }
            else
            {
                $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PW);
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            }

            $this->xmlDoc = $xmlDoc;
            $this->xpath = new DOMXPath($this->xmlDoc);
        }

        // Deletes everything in the DB so it can store again if you reload the page
        public function deleteRecords()
        {
            $stmt = $this->db->prepare('DELETE from city');
            $stmt->execute();
            $stmt = $this->db->prepare('DELETE from skierrecord');
            $stmt->execute();
            $stmt = $this->db->prepare('DELETE from club');
            $stmt->execute();
            $stmt = $this->db->prepare('DELETE from skier');
            $stmt->execute();
        }

        // Save the skier information to the DB
        public function extractSkiers()
        {
            $x = $this->xpath;
            $query = '//SkierLogs/Skiers/Skier';
            $entries = $x->query($query);
           
            foreach ($entries as $entry)
            {
                $userName = $entry->getAttribute("userName");
                $firstName = $entry->childNodes->item(1)->nodeValue;
                $lastName = $entry->childNodes->item(3)->nodeValue;
                $yearOfBirth = $entry->childNodes->item(5)->nodeValue;
                $skier = new Skier($userName, $firstName, $lastName, $yearOfBirth);

                $stmt = $this->db->prepare('SELECT userName FROM skier WHERE userName= :userName');
                $stmt->bindValue(':userName', $skier->userName, PDO::PARAM_STR);
                $stmt->execute();
                $stmt = $this->db->prepare('INSERT INTO skier(userName, firstName, lastName, yearOfBirth) VALUES(:userName, :firstName, :lastName, :yearOfBirth)');
                $stmt->bindValue(':userName', $skier->userName, PDO::PARAM_STR);
                $stmt->bindValue(':firstName', $skier->firstName, PDO::PARAM_STR);
                $stmt->bindValue(':lastName', $skier->lastName, PDO::PARAM_STR);
                $stmt->bindValue(':yearOfBirth', $skier->yearOfBirth, PDO::PARAM_INT);
                $stmt->execute();
            }
        }

         // Save the club information to the DB
        public function extractClubs()
        {
            $x = $this->xpath;
            $query = '//SkierLogs/Clubs/Club';
            $entries = $x->query($query);

            foreach ($entries as $entry)
            {
                $id = $entry->getAttribute("id");
                $name = $entry->childNodes->item(1)->nodeValue;
                $city = $entry->childNodes->item(3)->nodeValue;
                $club = new Club($id, $name, $city);

                $stmt = $this->db->prepare('INSERT INTO club(id, name, city) VALUES(:id, :name, :city)');
                $stmt->bindValue(':id', $club->id, PDO::PARAM_STR);
                $stmt->bindValue(':name', $club->name, PDO::PARAM_STR);
                $stmt->bindValue(':city', $club->city, PDO::PARAM_STR);
                $stmt->execute();
            }
        }

        // Save the city and county to the DB
        public function extractCities()
        {
            $x = $this->xpath;
            $query = '//SkierLogs/Clubs/Club';
            $entries = $x->query($query);

            foreach ($entries as $entry)
            {
                $city = $entry->childNodes->item(3)->nodeValue;
                $county = $entry->childNodes->item(5)->nodeValue;
                $city = new City($city, $county);

                $stmt = $this->db->prepare('INSERT INTO city(city, county) VALUES(:city, :county)');
                $stmt->bindValue(':city', $city->city, PDO::PARAM_STR);
                $stmt->bindValue(':county', $city->county, PDO::PARAM_STR);
                $stmt->execute();
            }
            
        }

        // Save a skiers season information: which club (if any) and total distance that given season
        public function extractSkierRecord()
        {
            $x = $this->xpath;
            $query = '//SkierLogs/Season/Skiers/Skier';
            $entries = $x->query($query);
            foreach ($entries as $entry)
            {
                $season = $entry->parentNode->parentNode->getAttribute("fallYear"); 
                $club = $entry->parentNode->getAttribute("clubId");
                
                if(empty($club))
                {
                    $club = null;
                }
        
                $userName = $entry->getAttribute("userName");
                $totalDistance = $this->calculateDistance($userName, $season);
                $skierRecord = new SkierRecord($userName, $season, $club, $totalDistance);
                
                $stmt = $this->db->prepare('INSERT INTO skierrecord(userName, season, club, distance) VALUES(:userName, :season, :club, :distance)');
                $stmt->bindValue(':userName', $skierRecord->userName, PDO::PARAM_STR);
                $stmt->bindValue(':season', $skierRecord->season, PDO::PARAM_INT);
                $stmt->bindValue(':club', $skierRecord->club, PDO::PARAM_STR);
                $stmt->bindValue(':distance', $skierRecord->distance, PDO::PARAM_INT);
                $stmt->execute();
            }
        }

        // Calculates the total distance in a season for a skier
        public function calculateDistance($userName, $season)
        {
            $totalDistance = 0;
            $x = $this->xpath;
            $Distances = $x->query('//SkierLogs/Season[@fallYear="'.$season.'"]/Skiers/Skier[@userName ="'.$userName.'"]//Distance');
            
            foreach($Distances as $Distance)
            {
                $totalDistance += $Distance->nodeValue;
            }
            return $totalDistance;
        }
    }
?>