<?php

    class SkierRecord {
        public $userName;
        public $season;
        public $club;
        public $distance;
        
        
        /** Constructor
        *@param string $userName Skier username
        *@param integer $season Season year
        *@param string $club Ski club (optional)
        *@param integer $distance Total distance that season
        */

        public function __construct($userName, $season, $club, $distance)
        {
            $this->userName = $userName;
            $this->season = $season;
            $this->club = $club;
            $this->distance = $distance;
        }
    
    }

?>