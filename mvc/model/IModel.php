<?php
    Interface IModel 
    {
        public function deleteRecords();
        public function extractSkiers();
        public function extractClubs();
        public function extractCities();
        public function extractSkierRecord();
    }
?>