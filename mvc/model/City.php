<?php
    
    class City {
        public $city;
        public $county;
    

        /**
         * @param string $city Club city
         * @param string $county Club county
         */

        public function __construct($city, $county)
        {
            $this->city = $city;
            $this->county = $county;
        }
    }

?>