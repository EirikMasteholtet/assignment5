<?php

class Club {
    public $id;
    public $name;
    public $city;

    /**
     * @param string $id Club id
     * @param string $name Club name
     * @param string $city Club location
     */

    public function __construct($id, $name, $city)
    {
        $this->id = $id;
        $this->name = $name;
        $this->city = $city;
    }
}

?>